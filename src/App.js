// import logo from './logo.svg';
import './App.css';
import history from './history'

import {
  Router,
  Route,
  Switch
} from 'react-router-dom'
import routes from './routes';

function App() {
  return (
    <Router history={history}>
        <div style={{backgroundColor:'lightgrey'}}>
          {/* <TopMenu/> */}
          <div className="App" style={{backgroundColor:'lightgrey'}}>
            <Switch>
              {
                routes.map(
                  (route,index) => (
                    <Route
                        key={index}
                        exact={route.exact}
                        path={route.path}
                        component={route.component}
                    />
                  )
                )
              }
            </Switch>
          </div>
        </div>
        
      </Router>
  );
}

export default App;
