import {authTypes as types }from '../actions';

import {handleActions} from "redux-actions"

const initialState={
    userData:null,
    accessLevels:null,
    isAuthenticated:null
}

export default handleActions({


    [types.AUTHENTICATE]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:null
    }),
    [types.AUTHENTICATE_SUCCESS]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:true
    }),
    [types.AUTHENTICATE_FAIL]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:false
    }),
    
    [types.CLEAR_AUTH]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:null
    }),



},initialState)