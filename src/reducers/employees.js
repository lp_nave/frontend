import {empTypes as types }from '../actions';

import {handleActions} from "redux-actions"

const initialState={
    userData:null,
}

export default handleActions({


    [types.GET_EMPLOYEES]:(state,{payload})=>({
        ...state,loading:true, userData:null
    }),
    [types.GET_EMPLOYEES_SUCCESS]:(state,{payload})=>({
        ...state,loading:true, userData:payload
    }),
    [types.GET_EMPLOYEES_FAIL]:(state,{payload})=>({
        ...state,loading:true, userData:null
    }),


},initialState)