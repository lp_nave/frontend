import {combineReducers} from 'redux'

import authenticate from './authenticate'
import employees from './employees'



const rootReducer = combineReducers({
    Authenticate:authenticate,
    Employees:employees
})

export default rootReducer;