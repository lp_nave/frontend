import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import { authActions } from '../actions';

export class Login extends Component {

    constructor(props){
        super(props);
        this.state={
            email:"",
            password:"",
            loading:false,
            error:false
        }
       
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.isAuth===true){
            debugger
            nextProps.history.push("/List")

        }else if(nextProps.isAuth===false){
            debugger
            return{
                error:true,
                loading:false
            }
        }
        else{
            return null
        }
    }

    handleEmail=(e,{value})=>{
        this.setState({email:value})
    }

    handlePassword=(e,{value})=>{
        this.setState({password:value})
    }

    login=()=>{
        this.props.clearAuth()
        this.setState({loading:true,error:false})
        this.props.authenticate(this.state)
    }

    render() {
        return (
            <div style={{
                width: "100%",
                height: '100%',
                display: 'inline-block'
            }}>
                <Grid textAlign='center' style={{ height: '120vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}>

                    <Form onSubmit={this.login} size='large'>
                        <Segment stacked>
                            <Header as='h1' color='orange' textAlign='center'>
                                Log-in to your account
                            </Header>
                            
                        {/* <Form.Input onChange={this.handleEmail} type="email"fluid icon='user' iconPosition='left' placeholder='E-mail address' /> */}
                        <Form.Input 
                            onChange={this.handleEmail}
                            fluid 
                            required
                            icon='user' 
                            iconPosition='left' 
                            placeholder='E-mail address' 
                            type='email'
                            />
                        <Form.Input
                            onChange={this.handlePassword}
                            fluid
                            required
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                        />
                          <Message
                                visible={this.state.error}
                                error
                                icon='x'
                                header='ERROR!'
                                content='Either your email or password is wrong. Please check and try again'
                            />

                        <Button 
                            disabled={this.state.email!=="" && this.state.password!==""?false:true}
                            loading={this.state.loading} 
                            // onClick={this.login} 
                            type='submit'
                            inverted 
                            color='orange'
                            fluid 
                            size='large'>
                            Login
                        </Button>
                        </Segment>
                    </Form>
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        ...state.Authenticate,
        isAuth:state.Authenticate.isAuthenticated,
    }
}

export default withRouter(connect(mapStateToProps,authActions)(Login))
