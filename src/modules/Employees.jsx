import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { CardGroup, Grid } from 'semantic-ui-react'
import DataCard from '../components/DataCard'
import {empActions} from '../actions'
import { bindActionCreators } from 'redux'

export class Employees extends Component {

    constructor(props){
        super(props);
        this.state={
            data:null
        }
    }

    componentDidMount(){
        this.props.empActions.getEmployees();
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.list!==null && nextProps.list!== prevState.data){
            return{
                data: nextProps.list
            }
        }
        else return null
    }



    render() {
        console.log(this.state.data)
        return (
            <div style={{padding:'10vh', height:'120vh'}}>
                <CardGroup itemsPerRow={4} centered>
                    {this.state.data!==null && this.state.data.map((item, index)=>{
                        return <DataCard key={index} data={item}/>
                    })}
                </CardGroup>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        empActions: bindActionCreators(empActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        list:state.Employees.userData
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (Employees))
