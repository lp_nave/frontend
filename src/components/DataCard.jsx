import React, { Component } from 'react'
import { Card, Icon, Image } from 'semantic-ui-react'

export class DataCard extends Component {
    render() {
        return (
            <div style={{padding:'5vh'}}>
                <Card raised>
                    <Image src={this.props.data.photo} wrapped ui={false} />
                    <Card.Content>
                    <Card.Header>{this.props.data.name}</Card.Header>
                    <Card.Meta>
                        <span className='date'>{this.props.data.email}</span>
                    </Card.Meta>
                    <Card.Description>
                        Branch: {this.props.data.branchName}
                    </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                    <a>
                        <Icon name='building' />
                        {this.props.data.bankName}
                    </a>
                    </Card.Content>
                </Card>
            </div>
        )
    }
}

export default DataCard
