import * as Auth from './Authenticate';
import * as Emp from './Employees';


export {
    Auth as authTypes
}
export const authActions = Auth.default;

export {
    Emp as empTypes
}
export const empActions = Emp.default;

