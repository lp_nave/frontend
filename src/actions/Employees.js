import {createAction} from 'redux-actions'

const action_header= "user/"


export const GET_EMPLOYEES = action_header+"GET_EMPLOYEES";
export const GET_EMPLOYEES_SUCCESS = action_header+"GET_EMPLOYEES_SUCCESS";
export const GET_EMPLOYEES_FAIL = action_header+"GET_EMPLOYEES_FAIL";



export default { 

    getEmployees: createAction(GET_EMPLOYEES),
    getEmployeesSuccess: createAction(GET_EMPLOYEES_SUCCESS),
    getEmployeesFail: createAction(GET_EMPLOYEES_FAIL),

}