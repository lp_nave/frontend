import {createAction} from 'redux-actions'

const action_header= "user/"


export const AUTHENTICATE = action_header+"AUTHENTICATE";
export const AUTHENTICATE_SUCCESS = action_header+"AUTHENTICATE_SUCCESS";
export const AUTHENTICATE_FAIL = action_header+"AUTHENTICATE_FAIL";

export const CLEAR_AUTH = action_header+"CLEAR_AUTH";


export default { 

    authenticate: createAction(AUTHENTICATE),
    authenticateSuccess: createAction(AUTHENTICATE_SUCCESS),
    authenticateFail: createAction(AUTHENTICATE_FAIL),

    clearAuth:createAction(CLEAR_AUTH),
}