import Employees from './modules/Employees'
import Login from './modules/Login'

const routes=[
    {
        path:'/',
        exact:true,
        component:Login
    },
    {
        path:'/List',
        exact:true,
        component:Employees
    },
]

export default routes