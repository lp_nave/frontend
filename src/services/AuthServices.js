import {createLogic} from 'redux-logic'

import {authActions, authTypes} from "../actions"
import jwtDecode from 'jwt-decode';
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const authenticate = createLogic({
    type:authTypes.AUTHENTICATE,
    latest:true,
    debounce:1000,

    process({
        action
    }, dispatch,done){

        let HTTPClient = API

        let obj ={
            email:action.payload.email,
            password: action.payload.password
        }
       
        HTTPClient.Post(endPoints.AUTHENTICATE , obj)
            // .then(resp=> resp.data)
            .then(resp=>{
                let decodedToken = jwtDecode(resp.data.jwtToken);
                console.log("decoded", decodedToken)
                debugger
                localStorage.setItem("jwt",resp.data.jwtToken);
                localStorage.setItem("email",decodedToken.sub);
                HTTPClient.setAuth();
                dispatch(authActions.authenticateSuccess(resp.data))
            })
            .catch(err=>{
                debugger
                var errormsg="Failed to Login";
                if (err && err.code === "ECONNABORTED") {
                    errormsg = "Please check your internet connection.";
                }
                dispatch(authActions.authenticateFail(errormsg))
            })
            .then(() => done());
    }
})

export default [
    authenticate,
]