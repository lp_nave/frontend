import {createLogic} from 'redux-logic'

import {empActions, empTypes} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const getEmployees = createLogic({
    type:empTypes.GET_EMPLOYEES,
    latest:true,
    debounce:1000,

    process({
        action
    }, dispatch,done){

        let HTTPClient = API
       
        HTTPClient.Get(endPoints.GET_EMPLOYEES)
            // .then(resp=> resp.data)
            .then(resp=>{
                dispatch(empActions.getEmployeesSuccess(resp.data))
            })
            .catch(err=>{
                debugger
                var errormsg="Failed to Login";
                if (err && err.code === "ECONNABORTED") {
                    errormsg = "Please check your internet connection.";
                }
                dispatch(empActions.getEmployeesFail(errormsg))
                window.alert("Error getting data")
            })
            .then(() => done());
    }
})

export default [
    getEmployees,
]