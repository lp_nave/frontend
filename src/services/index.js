import AuthServices from './AuthServices'
import EmpService from './EmpService'

export default [
    ...AuthServices,
    ...EmpService,

]