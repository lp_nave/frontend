import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware,compose} from 'redux';
import {createLogicMiddleware} from 'redux-logic'

import './index.css';

import services from './services';
import reducers from './reducers';

import App from './App';
import * as serviceWorker from './serviceWorker';

import 'semantic-ui-css/semantic.min.css'

//Creates redux-logic middleware
const logicMiddleware = createLogicMiddleware(services,{});

//Middlewares: applymiddleware() tells createStore() how to handle the middleware
const middleware = applyMiddleware(logicMiddleware)

//create enhancer for debugging the redux
const composeEnhancer =window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancer(middleware);

//cretae store and use the enhancer to handle the debugging
let store = createStore(reducers, enhancer);

//must use provider to add the store to the app
const app = (
    <Provider store={store}>
        <App/>
    </Provider>
)


ReactDOM.render(app, document.getElementById('root'));
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
serviceWorker.unregister();
